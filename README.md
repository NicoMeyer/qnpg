# Quantum Natural Policy Gradients
## Towards Sample-Efficient Reinforcement Learning

The python toolbox implementing the methods described in ["Quantum Natural Policy Gradients: Towards Sample-Efficient Reinforcement Learning", Meyer et al., IEEE QCE 2:36-41 (2023)](https://arxiv.org/abs/2304.13571) has been moved to [this Github repo](https://github.com/nicomeyer96/quantum-natural-policy-gradients).
